---
Week: 08
Content: Project part 1 phase 2
Material: See links in weekly plan
Initials: MON/NISI
---

# Week 08 Consolidation

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* All groups have presented their project as specified in the ww07 plan
* All groups has defined tasks to consolidate, complete documentation and finish deliverables.

  This must include a `deliverable` label that is added to all tasks that result in a delivery.

### Learning goals
* Technical group presentatation
    * Level 1: The group can present a project
    * Level 2: The group can do a presentation for a given audience
    * Level 3: The group can use various presentation means to design and adapt a presentation for different purposes and audiences

## Deliverables
* Mandatory weeky meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)

* Temperature readout in celcius from the temperature sensor (details in exercises document)
* Raspberry API Server update to be able to serve temperature readings (details in exercises document)


## Schedule

Monday

* 8:15 Introdution to the day, general Q/A session

    This wil include a discussion/reminder of what is important in a good presentation.

    How do we give feedback to presenters? how do presenters ask for feedback?

* 8:30 Group morning meeting

    You will meet in your groups every morning and decide on who does what. This is part of the project management, and is mandatory.
    Ordinary agenda:
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.

* 9:00 You work on deliverables

    Remember to come ask questions if you have any.   

* 12:30 Group presentations

    Remember to state: Purpose (you+audience), audience and duration

Tuesday

* 8:15 Introdution to the day, general Q/A session

* 8:30 Group morning meeting (See monday for agenda)

* 10:00 Teacher meetings

    Timeslot for you weekly 10 min. meeting with the teachers.

    Remember to book a time and have an agenda prepared.

* 9:00 You work on deliverables.

    Come ask us if you have questions.

* 12:30 3.sem elective subjects presentation

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19s-itt2-project/19S_ITT2_exercises.pdf) for details.

## Comments

* REST API stuff should be known. See resources from december.
