# Exercises for ww05


## Exercise 5 - Build veroboard with tempsensor, button and led 

### Information

In this exercise you will build a verobard with a temperature sensor, a button and an led.
The board will be connected to GPIO pins on an atmega328 the uses a 5V supply voltage.

### Exercise instructions

1. Read the schematic for the board via the link below. The repository contains both an Orcad project as well as a pdf file
2. Read the datasheet for the TMP36 temperature sensor
3. calculate the series resistor for the led
3. Make a veroboard layout in fritzing (install it if needed)
4. Build the board
5. Test that the led lights up when +5V is supplied to it.
6. Test that the button connects to ground when pressed (use a multimeter)
7. Test that the TMP36GT9Z output voltage changes according to the datasheet, when pinched between your fingers (see test link for inspiration)

### Links

* Schematic : [https://gitlab.com/atmega328-sensor-sample/schematics-orcad](https://gitlab.com/atmega328-sensor-sample/schematics-orcad)
* TMP36GT9Z datasheet: [https://docs-emea.rs-online.com/webdocs/0ae1/0900766b80ae1656.pdf](https://docs-emea.rs-online.com/webdocs/0ae1/0900766b80ae1656.pdf)
* TMP36GT9Z testing: [https://learn.adafruit.com/tmp36-temperature-sensor/testing-a-temp-sensor](https://learn.adafruit.com/tmp36-temperature-sensor/testing-a-temp-sensor)
* Fritzing: [http://fritzing.org/download/](http://fritzing.org/download/)

